<?php
/**
 * User: Sezare
 * Date: 17.12.2017
 * Time: 10:53
 */
class Application extends System\BaseApplication
{
  /**
   * @var array Конфигурация подключения к БД
   */
  private $dbConfig = array(
    "host" => "127.0.0.1",
    "port" => "3306",
    "name" => "",
    "user" => "root",
    "pass" => ""
  );

  /**
   * @var Экземпляр System\DB
   */
  public $db = false;

  /**
   * Подготовка приложения к запуску
   */
  public function load()
  {
    $this->db = new \System\DB();
    $dbStatus = $this->db->connect(
      $this->dbConfig["host"],
      $this->dbConfig["port"],
      $this->dbConfig["name"],
      $this->dbConfig["user"],
      $this->dbConfig["pass"]
    );
    if (!$dbStatus) {
      die($this->db->getLastError());
    }
  }
}