<?php
define("DOCROOT", $_SERVER["DOCUMENT_ROOT"]);

require DOCROOT . "/Core/DB.php";
require DOCROOT . "/Core/BaseObject.php.php";
require DOCROOT . "/Core/Router.php";
require DOCROOT . "/Core/BaseModel.php";
require DOCROOT . "/Core/BaseView.php";
require DOCROOT . "/Core/BaseController.php";
require DOCROOT . "/Core/Registry.php";
require DOCROOT . "/Core/BaseApplication.php";


spl_autoload_register(function ($class) {
  require DOCROOT. '/Classes/' . $class . '.php';
});

if (file_exists(DOCROOT . "/Content/Application.php")) {
  require DOCROOT . "/Content/Application.php";
  $app = new \Application();
} else {
  $app = new System\BaseApplication();
}
$app->run();