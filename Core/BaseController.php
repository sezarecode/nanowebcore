<?php
/**
 * Базовый контроллер запроса
 * User: Sezare
 * Date: 04.12.2017
 * Time: 12:02
 */

namespace System;

class BaseController
{
  /**
   * @var Модель запроса
   */
  public $model;

  /**
   * @var Вид запроса
   */
  public $view;

  /**
   * @var Ядро приложения
   */
  protected $app;

  /**
   * BaseController конструктор
   * @param $app - ядро приложения
   */
  public function __construct($app)
  {
    $this->app = $app;
  }

  public function invalidRequest()
  {
    die("Запрошенная страница не найдена");
  }
}