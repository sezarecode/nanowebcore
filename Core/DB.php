<?php
/**
 * User: Sezare
 * Date: 16.12.2017
 * Time: 11:29
 */
namespace System;

/**
 * Class DB
 * @package System
 */
class DB
{
  /**
   * @var PDO database link
   */
  private $link = false;

  /**
   * @var Кодировка подключения к базе данных
   */
  private $charset = "utf8";

  /**
   * @var Текстовое сообщение последней ошибки запроса к БД
   */
  private $lastError = false;

  /**
   * Подключение к базе данных
   * @param $host - адрес хоста
   * @param $port - номер порта
   * @param $db - имя базы данных
   * @param $user - имя пользователя
   * @param $pass - пароль
   * @return true при успешном подключении
   */
  public function connect($host, $port, $db, $user, $pass)
  {
    $this->lastError = false;
    $dsn = "mysql: host=".$host."; dbname=".$db.";port=".$port.";charset=".$this->charset;
    try {
      $this->link = new \PDO($dsn, $user, $pass);
    } catch (\PDOException $e) {
      $this->lastError = $e->getMessage();
      return false;
    }
    $this->link->exec("SET NAMES " + $this->charset);
    return true;
  }

  /**
   * Выполнение запроса к БД
   * @param $sql - запрос к БД
   * @param $binds - параметры запроса
   * @return PDOStatement при успешном запросе, false - при ошибке
   */
  public function query($sql, $binds = array())
  {
    $this->lastError = false;
    if (!$this->link) {
      $this->lastError = "Invalid DB connection";
      return false;
    }
    $stmt = $this->link->prepare($sql);
    $stmt->execute($binds);
    if ($stmt->errorCode() == 0) {
      return $stmt;
    } else {
      $this->lastError = $stmt->errorInfo()[2];
      return false;
    }
  }

  /**
   * Получение значения одного элемента БД
   * @param $sql - запрос к БД
   * @param $binds - параметры запроса
   * @return Запрошенное значение или false при ошибке
   */
  public function getOne($sql, $binds = array())
  {
    $data = $this->query($sql, $binds);
    if ($data) {
      $res = $data->fetch(\PDO::FETCH_NUM);
      return $res[0];
    } else {
      return false;
    }
  }

  /**
   * Получение строки таблицы БД в виде ассоциативного массива
   * @param $sql - запрос к БД
   * @param $binds - параметры запроса
   * @return Массив key=>value или false при ошибке
   */
  public function getLine($sql, $binds = array())
  {
    $data = $this->query($sql, $binds);
    if ($data) {
      return $data->fetch(\PDO::FETCH_ASSOC);
    } else {
      return false;
    }
  }

  /**
   * Получение набора строк таблицы БД в виде ассоциативного массива
   * @param $sql - запрос к БД
   * @param $binds - параметры запроса
   * @return Массив [key=>value],[key=>value] или false при ошибке
   */
  public function getAll($sql, $binds = array())
  {
    $data = $this->query($sql, $binds);
    if ($data) {
      $lines = array();
      while ($line = $data->fetch(\PDO::FETCH_ASSOC)) {
        $lines[] = $line;
      }
      return $lines;
    } else {
      return false;
    }
  }

  /**
   * Получение текстового сообщения последней ошибки запроса к БД
   * @return текстовое сообщение или false при его отсутствии
   */
  public function getLastError()
  {
    return $this->lastError;
  }

}