<?php
/**
 * Глобальный реестр приложения
 * User: Sezare
 * Date: 04.12.2017
 * Time: 12:20
 */

namespace System;

class Registry
{
  /**
   * Получение значения ключа в реестре
   * @param $key - индекс ключа
   * @return mixed значение ключа
   */
  public function get($key)
  {
    return $this->data[$key];
  }

  /**
   * Запись значения ключа реестре
   * @param $key - индекс ключа
   * @param $value - значение ключа
   */
  public function set($key, $value)
  {
    $this->data[$key] = $value;
  }

  /**
   * @var Хранилище данных
   */
  private $data = array();
}