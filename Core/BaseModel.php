<?php
/**
 * User: Sezare
 * Date: 15.12.2017
 * Time: 18:37
 */
namespace System;

/**
 * Class BaseModel
 * @package System
 */
class BaseModel
{
  /**
   * BaseModel конструктор
   * @param $app - ядро приложения
   */
  public function __construct($app)
  {
    $this->app = $app;
  }
}