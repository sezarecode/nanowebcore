<?php
/**
 * Маршрутизатор запроса к приложению
 * User: Sezare
 * Date: 04.12.2017
 * Time: 11:30
 */

namespace System;

class Router
{
  /**
   * @var Ядро приложения
   */
  private $app;

  /**
   * Router конструктор
   * @param $app - ядро приложения
   */
  public function __construct($app)
  {
    $this->app = $app;
  }

  /**
   * Запуск маршрутизатора
   */
  public function run()
  {
    $requestId = $this->getRequestId();
    $requestPath = DOCROOT . "/Content/" . $requestId;
    $controllerPath = $requestPath . "/Controller.php";
    if (file_exists($controllerPath))
    {
      $modelPath = $requestPath . "/Model.php";
      $viewPath = $requestPath . "/View.php";
      $this->handleRequest($modelPath, $viewPath, $controllerPath);
    }
    else
    {
      $controller = new BaseController($this->app);
      $controller->invalidRequest();
    }
  }

  /**
   * Получение идентификатора запроса
   */
  private function getRequestId()
  {
    $uri = explode("?", htmlspecialchars($_SERVER["REQUEST_URI"], ENT_NOQUOTES));
    if ($uri[0][strlen($uri[0]) - 1] != '/') $uri[0] .= '/';
    $uri[0] .= "index";
    return $uri[0];
  }

  /**
   * Обработка корректного запроса
   * @note Создание экземпляров классов контроллера, модели и вида запроса
   * @param $modelPath - путь к классу модели запроса
   * @param $viewPath - путь к классу вида запроса
   * @param $controllerPath - путь к классу контроллера запроса
   */
  private function handleRequest($modelPath, $viewPath, $controllerPath)
  {
    require $controllerPath;
    $controller = new \Controller($this->app);
    if (file_exists($modelPath)) {
      require $modelPath;
      $controller->model = new \Model($this->app);
    } else {
      $controller->model = new BaseModel($this->app);
    }
    if (file_exists($viewPath)) {
      require $viewPath;
      $controller->view = new \View($this->app);
    } else {
      $controller->view = new BaseView($this->app);
    }
    $controller->run();
  }
}