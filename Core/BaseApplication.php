<?php
/**
 * Ядро приложения
 * User: Sezare
 * Date: 04.12.2017
 * Time: 12:16
 */

namespace System;

class BaseApplication
{
  /**
   * @var Маршрутизатор запросов к приложению
   */
  private $router;

  /**
   * @var Глобальный реестр приложения
   */
  protected $registry;

  /**
   * BaseApplication конструктор
   */
  public function __construct()
  {
    $this->registry = new Registry();
    $this->router = new Router($this);
  }

  /**
   * Запуск приложения
   */
  public function run()
  {
    if (method_exists($this, "load")) {
      $this->load();
    }
    $this->router->run();
  }
}