<?php
/**
 * User: Sezare
 * Date: 15.12.2017
 * Time: 18:30
 */
namespace System;

/**
 * Class BaseView
 * @package System
 */
class BaseView
{
  /**
   * BaseView конструктор
   * @param $app - ядро приложения
   */
  public function __construct($app)
  {
    $this->app = $app;
  }

  /**
   * Вывод данных в формате JSON
   * @param $data, array - массив данных для вывода
   */
  public function json($data)
  {
    header('Content-Type: application/json');
    echo json_encode($data);
  }
}