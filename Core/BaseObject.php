<?php
/**
 * User: Sezare
 * Date: 17.12.2017
 * Time: 14:03
 */
namespace System;

/**
 * Class BaseObject
 * @package System
 */
class BaseObject
{
  /**
   * @var Ссылка на экземпляр ядра приложения
   */
  protected $app = false;

  /**
   * BaseObject constructor
   * @param $app - ссылка на экземпляр ядра приложения
   */
  public function __construct($app)
  {
    $this->app = $app;
    if (method_exists($this, "activate")) {
      $this->activate();
    }
  }

  /**
   * Загрузка свойств объекта
   * @param $props, array( "property" => "value" )
   */
  public function loadProperties($props)
  {
    foreach ($props as $key => $value) {
      if (property_exists($this, $key)) {
        $this->$key = $value;
      }
    }
  }
}